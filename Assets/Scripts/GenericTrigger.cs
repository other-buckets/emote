﻿using UnityEngine.Playables;
using UnityEngine;

public class GenericTrigger : MonoBehaviour {
	
	public PlayableDirector timeline;

	void Start () {
		timeline = GetComponent<PlayableDirector> ();
	}
	
	// Update is called once per frame
	void OnTriggerExit (Collider c) {
		if(c.gameObject.tag == "player1") {
			timeline.Stop();
		}
	}

	void OnTriggerEnter(Collider c) {
		if (c.gameObject.tag == "player1") {
			timeline.Play();
		}
	}
}
