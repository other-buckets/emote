﻿using System.Collections;
using System;
using System.Text;
using System.Collections.Generic;
using UnityEngine;

public class InputBuffer : MonoBehaviour {

    public bool SuperSpeedActive = false;

    private Queue<string> Keys = new Queue<string>();
    private List<string> ValidKeys = new List<string>() { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };

    public void BufferInput() {
        if(!string.IsNullOrEmpty(Input.inputString.ToLower()) && IsValidKeyPress(Input.inputString.ToLower())) {
            AddKeyPress(Input.inputString.ToLower());
        }
        CheckForCheatCodes();
	}

    private bool IsValidKeyPress(string input) {
        return ValidKeys.Contains(input);
    }

    private void AddKeyPress(string input) {
        if(Keys.Count == 10) {
            Keys.Dequeue();
        }
        Keys.Enqueue(input);
    }

    private void CheckForCheatCodes() {
        if (Keys.Count < 3) return;

        var sb = new StringBuilder();
        foreach(var s in Keys) {
            sb.Append(s);
        }

        var code = sb.ToString();

        //Debug.Log(code);

        if(code.Contains("cheat")) {
            Debug.Log("Cheat code CHEAT has been detected!");
            Keys.Clear();
        }
        if(code.Contains("wtf")) {
            Debug.Log("You're a dick!");
            Keys.Clear();
        }
        if(code.Contains("superspeed")) {
            Debug.Log("Super Speed Activated!");
            SuperSpeedActive = true;
            Keys.Clear();
        }
    }
}
