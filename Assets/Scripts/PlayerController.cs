﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float walkSpeed = 2;
    public float runSpeed = 6;
	public float gravity = -12;
    public float relicDetectRadius = 2;

	public float turnSmoothTime = 0.2f;
	float turnSmoothVelocity;

	public float speedSmoothTime = 0.1f;
	float speedSmoothVelocity;
	float currentSpeed;
	float velocityY;

	Animator animator;
	Transform cameraT;
	CharacterController controller;

    private Inventory Inventory = new Inventory();
    private InputBuffer _inputBuffer = new InputBuffer();

	void Start () {

		animator = GetComponent<Animator> ();
		cameraT = Camera.main.transform;
		controller = GetComponent<CharacterController>();
	}

	void Update () {
        HandleMovement();
        if(Input.GetKey(KeyCode.F)) {
            CheckForInteraction();
        }
        _inputBuffer.BufferInput();
	}

    private void HandleMovement() {
        Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        Vector2 inputDir = input.normalized;

        if (inputDir != Vector2.zero)
        {
            float targetRotation = Mathf.Atan2(inputDir.x, inputDir.y) * Mathf.Rad2Deg + cameraT.eulerAngles.y;
            transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(transform.eulerAngles.y, targetRotation, ref turnSmoothVelocity, turnSmoothTime);
        }

        bool running = Input.GetKey(KeyCode.LeftShift);
        float targetSpeed = (running ? runSpeed : walkSpeed) * inputDir.magnitude * (_inputBuffer.SuperSpeedActive ? 6 : 1);
        currentSpeed = Mathf.SmoothDamp(currentSpeed, targetSpeed, ref speedSmoothVelocity, speedSmoothTime);

		velocityY += Time.deltaTime * gravity;
		Vector3 velocity = transform.forward * currentSpeed + Vector3.up * velocityY;

		controller.Move(velocity * Time.deltaTime);
		currentSpeed = new Vector2(controller.velocity.x, controller.velocity.z).magnitude;

		if(controller.isGrounded){
			velocityY = 0;
		}

		float animationSpeedPercent = ((running) ? currentSpeed / runSpeed : currentSpeed / walkSpeed * .5f);
        animator.SetFloat("speedPercent", animationSpeedPercent, speedSmoothTime, Time.deltaTime);
    }

    private void CheckForInteraction() {
        Collider[] objectsInRange = Physics.OverlapSphere(transform.position, relicDetectRadius);
        foreach(var obj in objectsInRange) {
            if (obj.tag != "Relic") continue;
            Destroy(obj.gameObject);
            Inventory.AddRelic();
            Debug.Log("You obtained a relic!");
        }
    }
}
