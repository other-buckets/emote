﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory {

    private int RelicsFound = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void AddRelic() {
        RelicsFound++;
        Debug.Log("Player has collected " + RelicsFound + " relics.");
    }
}
